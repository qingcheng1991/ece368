//programer: Qing Cheng
//ECE368
//Data: September 27, 2017
//Description:This function is going to create stack data sturcture and allow user to add from
//the top and remove element one by one from the top and as well as be able to reverse the elements
#include<iostream>
#include<cstdio>
#include<cstdlib>
#include <cassert>
using namespace std;

class stack
{
public:
    stack(){top=-1;element=new int[100];} // constructor
    bool empty() const; // returns true if the stack is empty
    int size() const; // returns the number of items in the stack
    void push(int n); // adds item n into the front of the stack
    int pop(); // removes and returns the item in the front of the stack
    void flip();//reverse the stack element
    
    
    
private:
  
    int top;
    int *element;
    
};
//to check if the stack is empty by look for if the top is -1
bool stack::empty()const{
    return (top==-1);
}
//to return the size of stack which is the value of top integer
int stack::size()const{
    return top;
}
//adding the elements into the stack and update the size of stack
void stack::push(int n){
    //check of the stack is over max capacity
    assert(top < 100-1);
    top++;
    element[top]=n;
}
//remove the top value and uodate the top value
int stack::pop(){
    assert(!empty());
    top--;
    return element[top];
}
//reverse the stack element the top element will start to bottom
void stack::flip(){
    if (empty())
    {
        return;
    }
    else
    {
        //creat new array to store the old stack
        int *temp;
        temp=new int[100];
        int i=-1;
        while(empty()!=true)
        {
            temp[i]=element[top];
            pop();
            i++;
        }
        //to store the old stack into reverse order
        while(i!=-1){
            push(temp[top]);
            i--;
            
        }
        //free the temp memeory
        delete [] temp;
    }
}



