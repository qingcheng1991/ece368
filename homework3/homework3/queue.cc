//Programer:Qing Cheng
//ECE368
//Date: September 27 2017
//Description: This program is creating queues to store and remove the data first in first out and as well as
//reverse whole queue
#include<iostream>
#include<cstdio>
#include<cstdlib>
#include <cassert>
using namespace std;

class queue
{
public:
    queue(){
         front = 0; rear = 0;
         element = new int[100];
    }// constructor
    bool empty() const{
        return (front==rear);
    }// returns true if the queue is empty
    int size() const{
        if ( rear>=front ) return rear-front;
        else return 100+rear-front;
    }// returns the number of items in the queue
    void enqueue(int n); // adds item n to the end of the queue
    int dequeue(); // removes and returns the item in the front of the queueint
    void flip();  // rever the queue which rear become front and front become rear
    
    
private:
    int rear,front;
    int *element;
};
//adding the new element into the rear and update the rear number
void queue::enqueue(int n){
    assert(size()<100);
    element[rear]=n;
    rear=(rear+1)%100;
}
//remove very first front element update the front number and return the new front element
int queue::dequeue(){
    element[front]=NULL;
    front=(front+1)%100;
    return element[front];
}
//reverse the queue
void queue::flip(){
    if (empty())
    {
        return;
    }
    else
    {
        //allocate the new memory for store the old queue elements
        int *temp;
        int i=rear-1;
        temp=new int[100];
        //store all the elemnets into the temp array
        while(size() !=0){
            temp[i]=element[front];
            dequeue();
            i--;
            
        }
        //reset the rear number to zero
        rear=0;
        //loop through and add the rear element into front to make the queue reverse
        while(i<front-1)
        {
            i++;
            enqueue(temp[i]);
            
            
        }
        //reset the front to zero
        front=0;
        //delete the temp array to free the memory
        delete [] temp;
    }
}



int main(){
    queue n;
    n.enqueue(1);
    n.enqueue(2);
    n.flip();
    
    
}
