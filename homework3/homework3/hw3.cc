//programer: Qing Cheng
//Data: September 26
//Description: this programing is using linked list to store data and remove data
#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;

template <typename T>
class node
{
public:
    T element;
    node* next;
    node(const T& e) { element = e; next = NULL; }
};
template <typename T>
class linkedlist
{
private:
    node<T>* head;
    //insert the data into the before the request pointer pointing to
    void insert_before(node<T>* p, node<T>* np);
    //remove the data from before the pointer pointing at
    void remove_before(node<T>* p);
    
public:
    
    linkedlist() { head = NULL; }
    //deconstructor to delect the every thing
    ~linkedlist(){
        for ( node<T>* p = head; p != NULL; )
        {
            node<T>* next = p->next;
            delete p;
            p = next;
        }
    }
    void push_back(node<T>* np);
    
    void remove_back();
    void display();
    
   
    
};
//constructor to set head to NULL to start from the begining
template <typename T>
void linkedlist<T>::insert_before(node<T>* p, node<T>* np){
    
    //link thoese two node togther
    p->next=np->next;
    //np are followed by pointer p
    np->next=p;
    
}
template <typename T>
void linkedlist<T>::remove_before(node<T>* p){
    
    node<T>*temp;
    int index=-1;
    if (head == NULL)
   
    
    temp = head;
    cout<<"Elements of list are: "<<endl;
    //counting the index to the one beofore the pointer p pointing at
    while (temp != p)
    {
        
        temp = temp->next;
        index++;
    }
    //remove the previous element where pointer p pointing at
    for(int i=0; i<index;i++){
         temp = temp->next;
    }
    temp->next=temp->next->next;
    
   
}
//adding one element into the back of the linked list
template <typename T>
void linkedlist<T>::push_back(node<T> *np){
    node<T> *s;
    node<T>* temp = new node<T>;
    //allocte the new memory for the new element and create new note
    if (temp == NULL)
    {
        cout<<"Memory not allocated "<<endl;
        return;
       
    }
    else
    {
        temp->element = *np;
        temp->next = NULL;
        return temp;
    }
    //loading through the end of linked list and add the elemnet at back
    s = head;
    while (s->next != NULL)
    {
        s = s->next;
    }
    temp->next = NULL;
    s->next = temp;
    cout<<"Element Inserted at last"<<endl;
    
    
}
//remove the last elemnet of linked list
template <typename T>
void linkedlist<T>::remove_back(){
    //loading through the linked the list unstill and last one and delete the last one
    if (head->next == NULL) {
        delete head;
        head = NULL;
    }
    else {
        node<T> *nextToEnd = head;
        node<T> *end = head->next;
        while (end->next != NULL) {
            nextToEnd = end;
            end = end->next;
        }
        delete end;
        //let the new last element pointing to the NULL
        nextToEnd->next = NULL;
    }
    
}

template <typename T>
void linkedlist<T>::display(){
    node<T>*temp;
    if (head == NULL)
    {
        cout<<"The List is Empty"<<endl;
        return;
    }
    temp = head;
    cout<<"Elements of list are: "<<endl;
    while (temp != NULL)
    {
        cout<<temp->element<<"->";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

int main(){
    linkedlist<int> n;
    // add several records into the directory
    
    
    
   
    
    
    
    
    
    
    
    
}
